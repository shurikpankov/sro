﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace sroSite.Models
{
    public class sroCall
    {
        [Key]
        public int Id { get; set; }
        public string Status { get; set; }
        public DateTime? StatTime { get; set; }
        public DateTime? EndTime { get; set; }
        public bool? LandlordReached { get; set; }
        public int? ContactId { get; set; } //Bad, not connected to sroLandlordContacts

        //Arguments
        public bool argGeneral { get; set; } = false;
        public bool argNationwide { get; set; } = false;
        public bool argMarket { get; set; } = false;
        public bool argLowMargin { get; set; } = false;
        public bool argNetwork { get; set; } = false;
        public bool argPriceDynamic { get; set; } = false;

        public string Comment { get; set; }        

        //calculated
        public string ShortDateString { get
            { return (StatTime.HasValue) ? StatTime.Value.ToShortDateString() : String.Empty; }
        }
        public string ShortStartTimeString
        {
            get
            { return (StatTime.HasValue) ? StatTime.Value.ToShortTimeString() : String.Empty; }
        }
        public string ShortCallLengthString
        {
            get
            { return (StatTime.HasValue && EndTime.HasValue) ? (EndTime - StatTime).Value.ToString().Split('.')[0] : String.Empty; }
        }

        public virtual List<sroCallsTechSites> CallsTechSites { get; set; }

        public virtual List<sroTask> Tasks { get; set; }
        public virtual List<sroProposal> Proposals { get; set; }
    }
}