﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace sroSite.Models
{
    public class sroTask
    {
        [Key]
        public int Id { get; set; }
        public string Type { get; set; } //"Information", "LetterEmail", "LetterPost", "Contract", "Escalation"
        public string Status { get; set; } //"Open", "Approval", "Complete"
        //public string SubStatus { get; set; } //"", "Open", "Complete"
        public string Responsible { get; set; } //UId
        public DateTime? StartDate { get; set; }
        public DateTime? LastChangeDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string TaskData { get; set; }
        public string ResponseData { get; set; }
        public string EcmLink { get; set; }
        public bool? Success { get; set; }

        public int CallId { get; set; }
        public virtual sroCall Call { get; set; }
    }
}