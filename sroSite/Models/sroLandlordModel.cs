﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace sroSite.Models
{
    public class sroLandlord
    {
        [Key]
        public int Id { get; set; }
        
        public string Name { get; set; }
        public string INN { get; set; }
        public string Type { get; set; }

        public virtual List<sroTechSite> Sites { get; set; }

        public virtual List<sroLandlordContact> Contacts { get; set; }
    }
}