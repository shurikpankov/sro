﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace sroSite.Models
{
    public class sroProposal
    {
        [Key]
        public int Id { get; set; }
        public decimal? DecreaseValue { get; set; }
        public decimal? DecreasePercent { get; set; }
        public DateTime? DecreaseStart { get; set; }
        public DateTime? DecreaseEnd { get; set; }

        public int Prepayment { get; set; } = 0;
        public bool HasAdditionalCosts { get; set; } = false;
        public bool IsLandlordProposal { get; set; } = false;

        public bool? Success { get; set; }

        public int CallId { get; set; }
        public virtual sroCall Call { get; set; }
    }
}