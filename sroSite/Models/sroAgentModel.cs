﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace sroSite.Models
{
    public class sroAgent
    {
        [Key]
        public int Id { get; set; }
        public string UId { get; set; }
        //public string Name { get; set; } - deleted
        public string Name
        {
            get
            { return (FIO.Split(' ').Length > 2) ? FIO.Split(' ')[1] : String.Empty; }
        }
        public string FIO { get; set; }
        public string Phone { get; set; }
        //public string Email { get; set; } - deleted

        public virtual List<sroTechSite> Sites { get; set; }
    }
}