﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace sroSite.Models
{
    public class sroTechSite
    {
        [Key]
        public int Id { get; set; }
        public string Pull { get; set; }

        public string ERP { get; set; }
        public string BSNum { get; set; }
        public string Region { get; set; }
        public string Filial { get; set; }
        public string Address { get; set; }
        public decimal? Latitude { get; set; }
        public decimal? Longitude { get; set; }

        public string ContractNumber { get; set; }
        public DateTime? ContractDate { get; set; }
        public DateTime? ContractExpire { get; set; }
        public bool? AutoLong { get; set; }
        public int? AutoLongPeriod { get; set; }
        public string SpecialContractTerms { get; set; }

        public decimal? PricePerMonth { get; set; }
        public decimal? PriceCompareToAvg { get; set; }
        public int? Tax { get; set; }
        public int? PaymentFrequency { get; set; }
        public bool? Prepayment { get; set; }

        public DateTime? LastChangeDate { get; set; }
        public decimal? LastChagePercent { get; set; }        

        public bool? aLowMarginSite { get; set; }
        public bool? aDecreasedEquipment { get; set; }
        public bool? aOftenGrowth { get; set; }
        public bool? aLoyalClient { get; set; }
        

        public bool? rImportantSite { get; set; }
        public bool? rElectricityIsIncludedInRent { get; set; }
        public bool? rBigAmountOfEqupment { get; set; }
        public bool? rIncreasedAmountIfEquipment { get; set; }        
        public bool? rDiscountsOrLongPriceFreeze { get; set; }
        public bool? rPaymentTrouble { get; set; }
        public bool? rServiceTroubleOrOffset { get; set; }

        public string Comment { get; set; }

        public string Status { get; set; } // "", "Not reached", "In progress", "Completed"
        public DateTime? StatusDate { get; set; }
        public string Result { get; set; } // "Not reached", "Growth", "Flat", "Saving" 
        public decimal? SavingAmount { get; set; }
        public decimal? SavingRelative { get; set; }
        public DateTime? SavingStartDate { get; set; }
        public DateTime? SavingEndDate { get; set; }
        public decimal? EffectTotal { get; set; }
        public decimal? EffectYear { get; set; }
        public decimal? Effect17 { get; set; }
        public decimal? Effect18 { get; set; }
        public decimal? EffectRep { get; set; }

        public bool? IsApproved { get; set; }

        public int AgentId { get; set; }
        public virtual sroAgent Agent { get; set; }

        public int NegotiatorId { get; set; }
        public virtual sroNegotiator Negotiator { get; set; }

        public int ContactId { get; set; }
        public virtual sroLandlordContact Contact { get; set; }

        public virtual List<sroCallsTechSites> CallsTechSites { get; set; }
    }
}