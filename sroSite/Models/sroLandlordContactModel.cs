﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace sroSite.Models
{
    public class sroLandlordContact
    {
        [Key]
        public int Id { get; set; }

        public string Person { get; set; }
        public string Position { get; set; }
        public string PhoneCell { get; set; }
        public string PhoneCity { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string PostAddress { get; set; }
        public bool? IsActual { get; set; }
        public DateTime? ActualizationDate { get; set; }
        public string Comment { get; set; }

        public int LandlordId { get; set; }
        public virtual sroLandlord Landlord { get; set; }
    }
}