﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sroSite.Models.Extensions
{
    public static class IdentityExtentions
    {
        public static string GetSroUserName()
        {
            if (HttpContext.Current.User.IsInRole("sroManager"))
                return HttpContext.Current.User.Identity.Name.ToString();

            if (HttpContext.Current.User.IsInRole("sroAgent"))
                return ApplicationDbContext.Create().sroAgents.Where(s => s.UId == HttpContext.Current.User.Identity.Name).FirstOrDefault().Name;

            if (HttpContext.Current.User.IsInRole("sroNegotiator"))
                return ApplicationDbContext.Create().sroNegotiators.Where(s => s.UId == HttpContext.Current.User.Identity.Name).FirstOrDefault().Name;

            return String.Empty;
        }

        public static string GetSroUserRolesStr()
        {
            string rolesStr = HttpContext.Current.User.Identity.Name.ToString();

            if (HttpContext.Current.User.IsInRole("sroManager"))
                rolesStr += " | Менеджер";

            if (HttpContext.Current.User.IsInRole("sroAgent"))
                rolesStr += " | Агент";

            if (HttpContext.Current.User.IsInRole("sroNegotiator"))
                rolesStr += " | Сопровождающий";

            return rolesStr;
        }
    }
}