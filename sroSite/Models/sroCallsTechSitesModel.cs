﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace sroSite.Models
{
    public class sroCallsTechSites
    {
        [Key, Column(Order = 1)]
        public int CallId { get; set; }
        [Key, Column(Order = 2)]
        public int TechSiteId { get; set; }

        //public decimal Price { get; set; }

        public virtual sroCall Call { get; set; }
        public virtual sroTechSite TechSite { get; set; }
    }
}