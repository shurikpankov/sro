﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(sroSite.Startup))]
namespace sroSite
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
