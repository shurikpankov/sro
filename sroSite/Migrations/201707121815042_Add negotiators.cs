namespace sroSite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Addnegotiators : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.sroNegotiators",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UId = c.String(),
                        Name = c.String(),
                        FIO = c.String(),
                        Phone = c.String(),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.sroTechSites", "NegotiatorId", c => c.Int(nullable: false));
            CreateIndex("dbo.sroTechSites", "NegotiatorId");
            AddForeignKey("dbo.sroTechSites", "NegotiatorId", "dbo.sroNegotiators", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.sroTechSites", "NegotiatorId", "dbo.sroNegotiators");
            DropIndex("dbo.sroTechSites", new[] { "NegotiatorId" });
            DropColumn("dbo.sroTechSites", "NegotiatorId");
            DropTable("dbo.sroNegotiators");
        }
    }
}
