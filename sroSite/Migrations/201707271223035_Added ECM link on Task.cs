namespace sroSite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedECMlinkonTask : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.sroTasks", "EcmLink", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.sroTasks", "EcmLink");
        }
    }
}
