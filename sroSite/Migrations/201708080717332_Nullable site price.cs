namespace sroSite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Nullablesiteprice : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.sroTechSites", "PricePerMonth", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("dbo.sroTechSites", "PriceCompareToAvg", c => c.Decimal(precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.sroTechSites", "PriceCompareToAvg", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.sroTechSites", "PricePerMonth", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
    }
}
