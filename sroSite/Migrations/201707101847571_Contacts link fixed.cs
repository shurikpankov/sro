namespace sroSite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Contactslinkfixed : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.sroLandlordContacts", "sroLandlord_Id", "dbo.sroLandlords");
            DropForeignKey("dbo.sroTechSites", "LandlordId", "dbo.sroLandlords");
            DropIndex("dbo.sroTechSites", new[] { "LandlordId" });
            DropIndex("dbo.sroLandlordContacts", new[] { "sroLandlord_Id" });
            RenameColumn(table: "dbo.sroLandlordContacts", name: "sroLandlord_Id", newName: "LandlordId");
            RenameColumn(table: "dbo.sroTechSites", name: "LandlordId", newName: "sroLandlord_Id");
            AlterColumn("dbo.sroTechSites", "sroLandlord_Id", c => c.Int());
            AlterColumn("dbo.sroLandlordContacts", "LandlordId", c => c.Int(nullable: false));
            CreateIndex("dbo.sroTechSites", "sroLandlord_Id");
            CreateIndex("dbo.sroLandlordContacts", "LandlordId");
            AddForeignKey("dbo.sroLandlordContacts", "LandlordId", "dbo.sroLandlords", "Id", cascadeDelete: true);
            AddForeignKey("dbo.sroTechSites", "sroLandlord_Id", "dbo.sroLandlords", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.sroTechSites", "sroLandlord_Id", "dbo.sroLandlords");
            DropForeignKey("dbo.sroLandlordContacts", "LandlordId", "dbo.sroLandlords");
            DropIndex("dbo.sroLandlordContacts", new[] { "LandlordId" });
            DropIndex("dbo.sroTechSites", new[] { "sroLandlord_Id" });
            AlterColumn("dbo.sroLandlordContacts", "LandlordId", c => c.Int());
            AlterColumn("dbo.sroTechSites", "sroLandlord_Id", c => c.Int(nullable: false));
            RenameColumn(table: "dbo.sroTechSites", name: "sroLandlord_Id", newName: "LandlordId");
            RenameColumn(table: "dbo.sroLandlordContacts", name: "LandlordId", newName: "sroLandlord_Id");
            CreateIndex("dbo.sroLandlordContacts", "sroLandlord_Id");
            CreateIndex("dbo.sroTechSites", "LandlordId");
            AddForeignKey("dbo.sroTechSites", "LandlordId", "dbo.sroLandlords", "Id", cascadeDelete: true);
            AddForeignKey("dbo.sroLandlordContacts", "sroLandlord_Id", "dbo.sroLandlords", "Id");
        }
    }
}
