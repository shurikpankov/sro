namespace sroSite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Taskadditionalparams : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.sroTasks", "SubStatus", c => c.String());
            AddColumn("dbo.sroTasks", "LastChangeDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.sroTasks", "LastChangeDate");
            DropColumn("dbo.sroTasks", "SubStatus");
        }
    }
}
