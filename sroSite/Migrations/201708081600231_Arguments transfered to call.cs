namespace sroSite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Argumentstransferedtocall : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.sroCalls", "argGeneral", c => c.Boolean(nullable: false));
            AddColumn("dbo.sroCalls", "argNationwide", c => c.Boolean(nullable: false));
            AddColumn("dbo.sroCalls", "argMarket", c => c.Boolean(nullable: false));
            AddColumn("dbo.sroCalls", "argLowMargin", c => c.Boolean(nullable: false));
            AddColumn("dbo.sroCalls", "argNetwork", c => c.Boolean(nullable: false));
            AddColumn("dbo.sroCalls", "argPriceDynamic", c => c.Boolean(nullable: false));
            DropColumn("dbo.sroProposals", "argGeneral");
            DropColumn("dbo.sroProposals", "argNationwide");
            DropColumn("dbo.sroProposals", "argMarket");
            DropColumn("dbo.sroProposals", "argLowMargin");
            DropColumn("dbo.sroProposals", "argNetwork");
            DropColumn("dbo.sroProposals", "argPriceDynamic");
        }
        
        public override void Down()
        {
            AddColumn("dbo.sroProposals", "argPriceDynamic", c => c.Boolean(nullable: false));
            AddColumn("dbo.sroProposals", "argNetwork", c => c.Boolean(nullable: false));
            AddColumn("dbo.sroProposals", "argLowMargin", c => c.Boolean(nullable: false));
            AddColumn("dbo.sroProposals", "argMarket", c => c.Boolean(nullable: false));
            AddColumn("dbo.sroProposals", "argNationwide", c => c.Boolean(nullable: false));
            AddColumn("dbo.sroProposals", "argGeneral", c => c.Boolean(nullable: false));
            DropColumn("dbo.sroCalls", "argPriceDynamic");
            DropColumn("dbo.sroCalls", "argNetwork");
            DropColumn("dbo.sroCalls", "argLowMargin");
            DropColumn("dbo.sroCalls", "argMarket");
            DropColumn("dbo.sroCalls", "argNationwide");
            DropColumn("dbo.sroCalls", "argGeneral");
        }
    }
}
