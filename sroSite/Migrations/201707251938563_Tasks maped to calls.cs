namespace sroSite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Tasksmapedtocalls : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.sroTasks", "CallId", c => c.Int(nullable: false));
            CreateIndex("dbo.sroTasks", "CallId");
            AddForeignKey("dbo.sroTasks", "CallId", "dbo.sroCalls", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.sroTasks", "CallId", "dbo.sroCalls");
            DropIndex("dbo.sroTasks", new[] { "CallId" });
            DropColumn("dbo.sroTasks", "CallId");
        }
    }
}
