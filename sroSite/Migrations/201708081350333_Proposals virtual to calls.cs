namespace sroSite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Proposalsvirtualtocalls : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.sroProposals",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DecreaseValue = c.Decimal(precision: 18, scale: 2),
                        DecreasePercent = c.Decimal(precision: 18, scale: 2),
                        DecreaseStart = c.DateTime(),
                        DecreaseEnd = c.DateTime(),
                        argGeneral = c.Boolean(),
                        argNationwide = c.Boolean(),
                        argMarket = c.Boolean(),
                        argLowMargin = c.Boolean(),
                        argNetwork = c.Boolean(),
                        argPriceDynamic = c.Boolean(),
                        Prepayment = c.Int(),
                        HasAdditionalCosts = c.Boolean(),
                        IsLandlordProposal = c.Boolean(),
                        Success = c.Boolean(),
                        CallId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.sroCalls", t => t.CallId, cascadeDelete: true)
                .Index(t => t.CallId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.sroProposals", "CallId", "dbo.sroCalls");
            DropIndex("dbo.sroProposals", new[] { "CallId" });
            DropTable("dbo.sroProposals");
        }
    }
}
