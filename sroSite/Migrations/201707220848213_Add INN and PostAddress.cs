namespace sroSite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddINNandPostAddress : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.sroLandlordContacts", "PostAddress", c => c.String());
            AddColumn("dbo.sroLandlords", "INN", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.sroLandlords", "INN");
            DropColumn("dbo.sroLandlordContacts", "PostAddress");
        }
    }
}
