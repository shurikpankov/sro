namespace sroSite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Tasksandregions : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.sroTasksTechSites",
                c => new
                    {
                        TaskId = c.Int(nullable: false),
                        TechSiteId = c.Int(nullable: false),
                        Call_Id = c.Int(),
                    })
                .PrimaryKey(t => new { t.TaskId, t.TechSiteId })
                .ForeignKey("dbo.sroTasks", t => t.Call_Id)
                .ForeignKey("dbo.sroTechSites", t => t.TechSiteId, cascadeDelete: true)
                .Index(t => t.TechSiteId)
                .Index(t => t.Call_Id);
            
            CreateTable(
                "dbo.sroTasks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Type = c.String(),
                        Status = c.String(),
                        StartDate = c.DateTime(),
                        EndDate = c.DateTime(),
                        TaskData = c.String(),
                        ResponseData = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.sroTechSites", "Region", c => c.String());
            AddColumn("dbo.sroTechSites", "Filial", c => c.String());
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.sroTasksTechSites", "TechSiteId", "dbo.sroTechSites");
            DropForeignKey("dbo.sroTasksTechSites", "Call_Id", "dbo.sroTasks");
            DropIndex("dbo.sroTasksTechSites", new[] { "Call_Id" });
            DropIndex("dbo.sroTasksTechSites", new[] { "TechSiteId" });
            DropColumn("dbo.sroTechSites", "Filial");
            DropColumn("dbo.sroTechSites", "Region");
            DropTable("dbo.sroTasks");
            DropTable("dbo.sroTasksTechSites");
        }
    }
}
