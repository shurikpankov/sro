namespace sroSite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class deletdtasktechsites : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.sroTasksTechSites", "Call_Id", "dbo.sroTasks");
            DropForeignKey("dbo.sroTasksTechSites", "TechSiteId", "dbo.sroTechSites");
            DropIndex("dbo.sroTasksTechSites", new[] { "TechSiteId" });
            DropIndex("dbo.sroTasksTechSites", new[] { "Call_Id" });
            DropTable("dbo.sroTasksTechSites");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.sroTasksTechSites",
                c => new
                    {
                        TaskId = c.Int(nullable: false),
                        TechSiteId = c.Int(nullable: false),
                        Call_Id = c.Int(),
                    })
                .PrimaryKey(t => new { t.TaskId, t.TechSiteId });
            
            CreateIndex("dbo.sroTasksTechSites", "Call_Id");
            CreateIndex("dbo.sroTasksTechSites", "TechSiteId");
            AddForeignKey("dbo.sroTasksTechSites", "TechSiteId", "dbo.sroTechSites", "Id", cascadeDelete: true);
            AddForeignKey("dbo.sroTasksTechSites", "Call_Id", "dbo.sroTasks", "Id");
        }
    }
}
