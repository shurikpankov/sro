namespace sroSite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedContactclass : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.sroLandlordContacts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Person = c.String(),
                        Position = c.String(),
                        PhoneCell = c.String(),
                        PhoneCity = c.String(),
                        Email = c.String(),
                        Address = c.String(),
                        IsActual = c.Boolean(),
                        sroLandlord_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.sroLandlords", t => t.sroLandlord_Id)
                .Index(t => t.sroLandlord_Id);
            
            AddColumn("dbo.sroTechSites", "ContactId", c => c.Int(nullable: false));
            CreateIndex("dbo.sroTechSites", "ContactId");
            AddForeignKey("dbo.sroTechSites", "ContactId", "dbo.sroLandlordContacts", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.sroLandlordContacts", "sroLandlord_Id", "dbo.sroLandlords");
            DropForeignKey("dbo.sroTechSites", "ContactId", "dbo.sroLandlordContacts");
            DropIndex("dbo.sroLandlordContacts", new[] { "sroLandlord_Id" });
            DropIndex("dbo.sroTechSites", new[] { "ContactId" });
            DropColumn("dbo.sroTechSites", "ContactId");
            DropTable("dbo.sroLandlordContacts");
        }
    }
}
