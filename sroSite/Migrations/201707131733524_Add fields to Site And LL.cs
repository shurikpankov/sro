namespace sroSite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddfieldstoSiteAndLL : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.sroTechSites", "BSNum", c => c.String());
            AddColumn("dbo.sroTechSites", "AutoLong", c => c.Boolean());
            AddColumn("dbo.sroTechSites", "AutoLongPeriod", c => c.Int());
            AddColumn("dbo.sroTechSites", "SpecialContractTerms", c => c.String());
            AddColumn("dbo.sroTechSites", "PriceCompareToAvg", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.sroTechSites", "Tax", c => c.Int());
            AddColumn("dbo.sroTechSites", "PaymentFrequency", c => c.Int());
            AddColumn("dbo.sroTechSites", "Prepayment", c => c.Boolean());
            AddColumn("dbo.sroTechSites", "LastChangeDate", c => c.DateTime());
            AddColumn("dbo.sroTechSites", "LastChagePercent", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.sroTechSites", "aLowMarginSite", c => c.Boolean());
            AddColumn("dbo.sroTechSites", "aDecreasedEquipment", c => c.Boolean());
            AddColumn("dbo.sroTechSites", "aOftenGrowth", c => c.Boolean());
            AddColumn("dbo.sroTechSites", "aLoyalClient", c => c.Boolean());
            AddColumn("dbo.sroTechSites", "rImportantSite", c => c.Boolean());
            AddColumn("dbo.sroTechSites", "rElectricityIsIncludedInRent", c => c.Boolean());
            AddColumn("dbo.sroTechSites", "rBigAmountOfEqupment", c => c.Boolean());
            AddColumn("dbo.sroTechSites", "rIncreasedAmountIfEquipment", c => c.Boolean());
            AddColumn("dbo.sroTechSites", "rDiscountsOrLongPriceFreeze", c => c.Boolean());
            AddColumn("dbo.sroTechSites", "rPaymentTrouble", c => c.Boolean());
            AddColumn("dbo.sroTechSites", "rServiceTroubleOrOffset", c => c.Boolean());
            AddColumn("dbo.sroTechSites", "Comment", c => c.String());
            AddColumn("dbo.sroLandlords", "Type", c => c.String());
            DropColumn("dbo.sroTechSites", "TechParams");
        }
        
        public override void Down()
        {
            AddColumn("dbo.sroTechSites", "TechParams", c => c.String());
            DropColumn("dbo.sroLandlords", "Type");
            DropColumn("dbo.sroTechSites", "Comment");
            DropColumn("dbo.sroTechSites", "rServiceTroubleOrOffset");
            DropColumn("dbo.sroTechSites", "rPaymentTrouble");
            DropColumn("dbo.sroTechSites", "rDiscountsOrLongPriceFreeze");
            DropColumn("dbo.sroTechSites", "rIncreasedAmountIfEquipment");
            DropColumn("dbo.sroTechSites", "rBigAmountOfEqupment");
            DropColumn("dbo.sroTechSites", "rElectricityIsIncludedInRent");
            DropColumn("dbo.sroTechSites", "rImportantSite");
            DropColumn("dbo.sroTechSites", "aLoyalClient");
            DropColumn("dbo.sroTechSites", "aOftenGrowth");
            DropColumn("dbo.sroTechSites", "aDecreasedEquipment");
            DropColumn("dbo.sroTechSites", "aLowMarginSite");
            DropColumn("dbo.sroTechSites", "LastChagePercent");
            DropColumn("dbo.sroTechSites", "LastChangeDate");
            DropColumn("dbo.sroTechSites", "Prepayment");
            DropColumn("dbo.sroTechSites", "PaymentFrequency");
            DropColumn("dbo.sroTechSites", "Tax");
            DropColumn("dbo.sroTechSites", "PriceCompareToAvg");
            DropColumn("dbo.sroTechSites", "SpecialContractTerms");
            DropColumn("dbo.sroTechSites", "AutoLongPeriod");
            DropColumn("dbo.sroTechSites", "AutoLong");
            DropColumn("dbo.sroTechSites", "BSNum");
        }
    }
}
