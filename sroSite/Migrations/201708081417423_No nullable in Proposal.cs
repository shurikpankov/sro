namespace sroSite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NonullableinProposal : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.sroProposals", "argGeneral", c => c.Boolean(nullable: false));
            AlterColumn("dbo.sroProposals", "argNationwide", c => c.Boolean(nullable: false));
            AlterColumn("dbo.sroProposals", "argMarket", c => c.Boolean(nullable: false));
            AlterColumn("dbo.sroProposals", "argLowMargin", c => c.Boolean(nullable: false));
            AlterColumn("dbo.sroProposals", "argNetwork", c => c.Boolean(nullable: false));
            AlterColumn("dbo.sroProposals", "argPriceDynamic", c => c.Boolean(nullable: false));
            AlterColumn("dbo.sroProposals", "Prepayment", c => c.Int(nullable: false));
            AlterColumn("dbo.sroProposals", "HasAdditionalCosts", c => c.Boolean(nullable: false));
            AlterColumn("dbo.sroProposals", "IsLandlordProposal", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.sroProposals", "IsLandlordProposal", c => c.Boolean());
            AlterColumn("dbo.sroProposals", "HasAdditionalCosts", c => c.Boolean());
            AlterColumn("dbo.sroProposals", "Prepayment", c => c.Int());
            AlterColumn("dbo.sroProposals", "argPriceDynamic", c => c.Boolean());
            AlterColumn("dbo.sroProposals", "argNetwork", c => c.Boolean());
            AlterColumn("dbo.sroProposals", "argLowMargin", c => c.Boolean());
            AlterColumn("dbo.sroProposals", "argMarket", c => c.Boolean());
            AlterColumn("dbo.sroProposals", "argNationwide", c => c.Boolean());
            AlterColumn("dbo.sroProposals", "argGeneral", c => c.Boolean());
        }
    }
}
