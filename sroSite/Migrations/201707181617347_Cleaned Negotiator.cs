namespace sroSite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CleanedNegotiator : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.sroNegotiators", "Name");
            DropColumn("dbo.sroNegotiators", "Email");
        }
        
        public override void Down()
        {
            AddColumn("dbo.sroNegotiators", "Email", c => c.String());
            AddColumn("dbo.sroNegotiators", "Name", c => c.String());
        }
    }
}
