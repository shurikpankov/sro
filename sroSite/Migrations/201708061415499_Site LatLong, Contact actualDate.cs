namespace sroSite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SiteLatLongContactactualDate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.sroTechSites", "Latitude", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.sroTechSites", "Longitude", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.sroLandlordContacts", "ActualizationDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.sroLandlordContacts", "ActualizationDate");
            DropColumn("dbo.sroTechSites", "Longitude");
            DropColumn("dbo.sroTechSites", "Latitude");
        }
    }
}
