namespace sroSite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSitePullandDeleteTaskSubStatus : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.sroTechSites", "Pull", c => c.String());
            DropColumn("dbo.sroTasks", "SubStatus");
        }
        
        public override void Down()
        {
            AddColumn("dbo.sroTasks", "SubStatus", c => c.String());
            DropColumn("dbo.sroTechSites", "Pull");
        }
    }
}
