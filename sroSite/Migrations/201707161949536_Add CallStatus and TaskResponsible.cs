namespace sroSite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCallStatusandTaskResponsible : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.sroCalls", "Status", c => c.String());
            AddColumn("dbo.sroTasks", "Responsible", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.sroTasks", "Responsible");
            DropColumn("dbo.sroCalls", "Status");
        }
    }
}
