namespace sroSite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CleanedAgent : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.sroAgents", "Name");
            DropColumn("dbo.sroAgents", "Email");
        }
        
        public override void Down()
        {
            AddColumn("dbo.sroAgents", "Email", c => c.String());
            AddColumn("dbo.sroAgents", "Name", c => c.String());
        }
    }
}
