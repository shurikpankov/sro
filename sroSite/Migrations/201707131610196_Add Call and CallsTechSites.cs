namespace sroSite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCallandCallsTechSites : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.sroCallsTechSites",
                c => new
                    {
                        CallId = c.Int(nullable: false),
                        TechSiteId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.CallId, t.TechSiteId })
                .ForeignKey("dbo.sroCalls", t => t.CallId, cascadeDelete: true)
                .ForeignKey("dbo.sroTechSites", t => t.TechSiteId, cascadeDelete: true)
                .Index(t => t.CallId)
                .Index(t => t.TechSiteId);
            
            CreateTable(
                "dbo.sroCalls",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StatTime = c.DateTime(),
                        EndTime = c.DateTime(),
                        LandlordReached = c.Boolean(),
                        Comment = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.sroCallsTechSites", "TechSiteId", "dbo.sroTechSites");
            DropForeignKey("dbo.sroCallsTechSites", "CallId", "dbo.sroCalls");
            DropIndex("dbo.sroCallsTechSites", new[] { "TechSiteId" });
            DropIndex("dbo.sroCallsTechSites", new[] { "CallId" });
            DropTable("dbo.sroCalls");
            DropTable("dbo.sroCallsTechSites");
        }
    }
}
