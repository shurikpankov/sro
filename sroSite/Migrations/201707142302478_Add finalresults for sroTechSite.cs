namespace sroSite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddfinalresultsforsroTechSite : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.sroTechSites", "Status", c => c.String());
            AddColumn("dbo.sroTechSites", "StatusDate", c => c.DateTime());
            AddColumn("dbo.sroTechSites", "Result", c => c.String());
            AddColumn("dbo.sroTechSites", "SavingAmount", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.sroTechSites", "SavingRelative", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.sroTechSites", "SavingStartDate", c => c.DateTime());
            AddColumn("dbo.sroTechSites", "SavingEndDate", c => c.DateTime());
            AddColumn("dbo.sroTechSites", "EffectTotal", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.sroTechSites", "EffectYear", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.sroTechSites", "Effect17", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.sroTechSites", "Effect18", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.sroTechSites", "EffectRep", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.sroTechSites", "IsApproved", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropColumn("dbo.sroTechSites", "IsApproved");
            DropColumn("dbo.sroTechSites", "EffectRep");
            DropColumn("dbo.sroTechSites", "Effect18");
            DropColumn("dbo.sroTechSites", "Effect17");
            DropColumn("dbo.sroTechSites", "EffectYear");
            DropColumn("dbo.sroTechSites", "EffectTotal");
            DropColumn("dbo.sroTechSites", "SavingEndDate");
            DropColumn("dbo.sroTechSites", "SavingStartDate");
            DropColumn("dbo.sroTechSites", "SavingRelative");
            DropColumn("dbo.sroTechSites", "SavingAmount");
            DropColumn("dbo.sroTechSites", "Result");
            DropColumn("dbo.sroTechSites", "StatusDate");
            DropColumn("dbo.sroTechSites", "Status");
        }
    }
}
