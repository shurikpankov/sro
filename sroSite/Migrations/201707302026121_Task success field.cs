namespace sroSite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Tasksuccessfield : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.sroTasks", "Success", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropColumn("dbo.sroTasks", "Success");
        }
    }
}
