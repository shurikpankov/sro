namespace sroSite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Saveingcontractpersononcall : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.sroCalls", "ContactId", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.sroCalls", "ContactId");
        }
    }
}
