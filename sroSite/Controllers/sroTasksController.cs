﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using sroSite.Models;

namespace sroSite.Controllers
{
    [Authorize]
    public class sroTasksController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: sroTasks
        public async Task<ActionResult> Index()
        {
            int page = 1;
            string search = Request["search"];
            int searchchanged = 0;
            IQueryable<sroTask> result = db.sroTasks.Include(s => s.Call);

            if (User.IsInRole("sroManager"))
            {
                result = db.sroTasks.Include(s => s.Call);
            }
            if (User.IsInRole("sroAgent"))
            {
                result = db.sroAgents.Where(a => a.UId == User.Identity.Name).SelectMany(x => x.Sites).SelectMany(s => s.CallsTechSites).Select(c => c.Call).SelectMany(t => t.Tasks).Distinct();
            }
            if (User.IsInRole("sroNegotiator"))
            {
                var sroTasksNegotiator = db.sroNegotiators.Where(n => n.UId == User.Identity.Name).SelectMany(x => x.Sites).SelectMany(s => s.CallsTechSites).Select(c => c.Call).SelectMany(t => t.Tasks).Distinct().ToList();
                var sroTasksResponsible = db.sroNegotiators.SelectMany(x => x.Sites).SelectMany(s => s.CallsTechSites).Select(c => c.Call).SelectMany(t => t.Tasks).Where(t => t.Responsible == User.Identity.Name).Distinct().ToList();
                foreach (sroTask respTask in sroTasksResponsible)
                {
                    if (sroTasksNegotiator.IndexOf(respTask) == -1)
                    {
                        sroTasksNegotiator.Add(respTask);
                    }
                }
                result = sroTasksNegotiator.AsQueryable();
            }

            if (!String.IsNullOrEmpty(Request["page"]) && int.TryParse(Request["page"], out page))
            {

            }
            else
            {
                page = 1;
            }

            if (!String.IsNullOrEmpty(Request["searchchanged"]) && Request["searchchanged"] == "1")
            {
                page = 1;
            }

           

            if (!string.IsNullOrEmpty(search))
            {
                result = result.Where(x => x.Call.CallsTechSites.FirstOrDefault().TechSite.Contact.Landlord.Name.Contains(search));
            }

            if (result != null)
            {
                ViewBag.TotalCount = result.Count();
            }
            else
            {
                ViewBag.TotalCount = 0;
            }

            result = result.OrderByDescending(x => x.Id).Skip(12 * (page - 1)).Take(12);

            ViewBag.page = page;
            return View(await result.ToListAsync());
        }

        // GET: sroTasks/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            sroTask sroTask = await db.sroTasks.FindAsync(id);
            if (sroTask == null)
            {
                return HttpNotFound();
            }
            return View(sroTask);
        }

        // GET: sroTasks/Create
        public ActionResult Create(int? callId)
        {
            if (!callId.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var call = db.sroCalls.Where(x => x.Id == callId).SingleOrDefault();
            if (call == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            ViewBag.CallId = call.Id;
            return View();
        }

        // POST: sroTasks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Type,Status,Responsible,StartDate,LastChangeDate,EndDate,TaskData,ResponseData,EcmLink,Success,CallId")] sroTask sroTask)
        {
            if (ModelState.IsValid)
            {
                sroTask.Status = "Новая";
                sroTask.StartDate = DateTime.Now;
                sroTask.LastChangeDate = sroTask.StartDate;
                db.sroTasks.Add(sroTask);
                await db.SaveChangesAsync();
                return RedirectToAction("Edit","sroCalls",new { Id = sroTask.CallId});
            }

            ViewBag.CallId = new SelectList(db.sroCalls, "Id", "Id", sroTask.CallId);
            return View(sroTask);
        }

        // GET: sroTasks/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (User.IsInRole("sroAgent"))
            {
                Response.Redirect(Url.Action("Index"));
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            sroTask sroTask = await db.sroTasks.FindAsync(id);
            if (sroTask == null)
            {
                return HttpNotFound();
            }
            if (sroTask.Status == "Завершена")
            {
                return RedirectToAction("Details", new { Id = sroTask.Id });
            }
            ViewBag.CallId = new SelectList(db.sroCalls, "Id", "Id", sroTask.CallId);
            ViewBag.Negotiators = new SelectList(db.sroNegotiators, "UId", "FIO", sroTask.Responsible);
            return View(sroTask);
        }

        // POST: sroTasks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Type,Status,Responsible,StartDate,LastChangeDate,EndDate,TaskData,ResponseData,EcmLink,Success,CallId")] sroTask sroTask)
        {
            var CurrTask = db.sroTasks.Where(x => x.Id == sroTask.Id).FirstOrDefault();

            bool? taskCompleteState = null;

            if(!string.IsNullOrEmpty(Request["Complete"]))
            {
                if (Request["Complete"] == "success")
                {
                    taskCompleteState = true;
                }
                if (Request["Complete"] == "negative")
                {
                    taskCompleteState = false;
                }
            }

            //sroTask.CallId = CurrTask.CallId;
            //sroTask.Type = CurrTask.Type;
            //sroTask.StartDate = CurrTask.StartDate;
            //sroTask.TaskData = CurrTask.TaskData;

            CurrTask.LastChangeDate = (sroTask.Status == CurrTask.Status) ? CurrTask.LastChangeDate : DateTime.Now;
            CurrTask.Status = sroTask.Status;
            CurrTask.Responsible = (String.IsNullOrEmpty(sroTask.Responsible)) ? null : sroTask.Responsible;
            CurrTask.ResponseData = sroTask.ResponseData;
            CurrTask.Success = taskCompleteState;

            if(sroTask.EcmLink != null)
            {
                CurrTask.EcmLink = sroTask.EcmLink;
            }

            if (CurrTask.Success != null)
            {
                CurrTask.Status = "Завершена";
                CurrTask.EndDate = DateTime.Now;
            }
            //only if not finished and not contract
            //sroTask.EcmLink = CurrTask.EcmLink;

            if (ModelState.IsValid)
            {
                db.Entry(CurrTask).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.CallId = new SelectList(db.sroCalls, "Id", "Id", sroTask.CallId);
            return View(sroTask);
        }

        // GET: sroTasks/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            sroTask sroTask = await db.sroTasks.FindAsync(id);
            if (sroTask == null)
            {
                return HttpNotFound();
            }
            return View(sroTask);
        }

        // POST: sroTasks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            sroTask sroTask = await db.sroTasks.FindAsync(id);
            db.sroTasks.Remove(sroTask);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
