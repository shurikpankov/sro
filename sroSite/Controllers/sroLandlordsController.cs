﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using sroSite.Models;

namespace sroSite.Controllers
{
    [Authorize]
    public class sroLandlordsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: sroLandlords
        public async Task<ActionResult> Index()
        {
            return View(await db.sroLandlords.ToListAsync());
        }

        // GET: sroLandlords/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            sroLandlord sroLandlord = await db.sroLandlords.FindAsync(id);
            if (sroLandlord == null)
            {
                return HttpNotFound();
            }
            return View(sroLandlord);
        }

        // GET: sroLandlords/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: sroLandlords/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name,INN,Type")] sroLandlord sroLandlord)
        {
            if (ModelState.IsValid)
            {
                db.sroLandlords.Add(sroLandlord);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(sroLandlord);
        }

        // GET: sroLandlords/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            sroLandlord sroLandlord = await db.sroLandlords.FindAsync(id);
            if (sroLandlord == null)
            {
                return HttpNotFound();
            }
            return View(sroLandlord);
        }

        // POST: sroLandlords/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name,INN,Type")] sroLandlord sroLandlord)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sroLandlord).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(sroLandlord);
        }

        // GET: sroLandlords/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            sroLandlord sroLandlord = await db.sroLandlords.FindAsync(id);
            if (sroLandlord == null)
            {
                return HttpNotFound();
            }
            return View(sroLandlord);
        }

        // POST: sroLandlords/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            sroLandlord sroLandlord = await db.sroLandlords.FindAsync(id);
            db.sroLandlords.Remove(sroLandlord);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
