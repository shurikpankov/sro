﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using sroSite.Models;

namespace sroSite.Controllers
{
    [Authorize]
    public class sroCallsTechSitesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: sroCallsTechSites
        public async Task<ActionResult> Index()
        {
            var sroCallsTechSites = db.sroCallsTechSites.Include(s => s.Call).Include(s => s.TechSite).Include(s => s.TechSite.Contact).Include(s => s.TechSite.Contact.Landlord).Include(s => s.TechSite.Agent);
            return View(await sroCallsTechSites.ToListAsync());
        }

        // GET: sroCallsTechSites/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            sroCallsTechSites sroCallsTechSites = await db.sroCallsTechSites.FindAsync(id);
            if (sroCallsTechSites == null)
            {
                return HttpNotFound();
            }
            return View(sroCallsTechSites);
        }

        // GET: sroCallsTechSites/Create
        public ActionResult Create()
        {
            ViewBag.CallId = new SelectList(db.sroCalls, "Id", "Comment");
            ViewBag.TechSiteId = new SelectList(db.sroTechSites, "Id", "ERP");
            return View();
        }

        // POST: sroCallsTechSites/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "CallId,TechSiteId")] sroCallsTechSites sroCallsTechSites)
        {
            if (ModelState.IsValid)
            {
                db.sroCallsTechSites.Add(sroCallsTechSites);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.CallId = new SelectList(db.sroCalls, "Id", "Comment", sroCallsTechSites.CallId);
            ViewBag.TechSiteId = new SelectList(db.sroTechSites, "Id", "ERP", sroCallsTechSites.TechSiteId);
            return View(sroCallsTechSites);
        }

        // GET: sroCallsTechSites/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            sroCallsTechSites sroCallsTechSites = await db.sroCallsTechSites.FindAsync(id);
            if (sroCallsTechSites == null)
            {
                return HttpNotFound();
            }
            ViewBag.CallId = new SelectList(db.sroCalls, "Id", "Comment", sroCallsTechSites.CallId);
            ViewBag.TechSiteId = new SelectList(db.sroTechSites, "Id", "ERP", sroCallsTechSites.TechSiteId);
            return View(sroCallsTechSites);
        }

        // POST: sroCallsTechSites/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "CallId,TechSiteId")] sroCallsTechSites sroCallsTechSites)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sroCallsTechSites).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.CallId = new SelectList(db.sroCalls, "Id", "Comment", sroCallsTechSites.CallId);
            ViewBag.TechSiteId = new SelectList(db.sroTechSites, "Id", "ERP", sroCallsTechSites.TechSiteId);
            return View(sroCallsTechSites);
        }

        // GET: sroCallsTechSites/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            sroCallsTechSites sroCallsTechSites = await db.sroCallsTechSites.FindAsync(id);
            if (sroCallsTechSites == null)
            {
                return HttpNotFound();
            }
            return View(sroCallsTechSites);
        }

        // POST: sroCallsTechSites/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            sroCallsTechSites sroCallsTechSites = await db.sroCallsTechSites.FindAsync(id);
            db.sroCallsTechSites.Remove(sroCallsTechSites);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
