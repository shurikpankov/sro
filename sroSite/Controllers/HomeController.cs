﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using sroSite.Models;

namespace sroSite.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public ActionResult Index()
        {
            var dbc = sroSite.Models.ApplicationDbContext.Create();


            List<int> siteStates = new List<int>();
            decimal savings = 0;
            for (int i = 0; i < 11; i++)
            {
                siteStates.Add(0);
            }
            List<sroTechSite> sites = dbc.sroTechSites.ToList();
            foreach (var site in sites)
            {
                switch (site.Status)
                {
                    case "Новый": siteStates[0] += 1; break;
                    case "Подготовка": siteStates[1] += 1; siteStates[3] += 1; break;
                    case "Дозвон": siteStates[1] += 1; siteStates[4] += 1; break;
                    case "Рассмотрение": siteStates[1] += 1; siteStates[5] += 1; break;
                    case "Реализация": siteStates[1] += 1; siteStates[6] += 1; break;
                    case "Снижение": siteStates[2] += 1; siteStates[7] += 1; break;
                    case "Удержание": siteStates[2] += 1; siteStates[8] += 1; break;
                    case "Рост": siteStates[2] += 1; siteStates[9] += 1; break;
                    case "Недозвон": siteStates[2] += 1; siteStates[10] += 1; break;
                    default: siteStates[0] += 1; break;
                }
                if (site.EffectRep != null && site.EffectRep.HasValue && site.EffectRep < 0)
                {
                    savings += site.EffectRep.Value;
                }
            }

            ViewBag.SiteStates = siteStates;
            ViewBag.Savings = savings;

            List<sroSite.Models.sroAgent> agents = dbc.sroAgents.ToList();

            List<int> sitesCount = new List<int>();
            List<int> finishedSitesCount = new List<int>();
            List<int> callsCount = new List<int>();

            foreach(var item in agents)
            { 
                var sroTechSitesAgent = db.sroTechSites.Include(s => s.Agent).Include(s => s.Contact.Landlord).Include(s => s.Contact).Include(s => s.Negotiator).Where(s => s.Agent.UId == item.UId);
                var sroTechSitesFinishedAgent = db.sroTechSites.Include(s => s.Agent).Include(s => s.Contact.Landlord).Include(s => s.Contact).Include(s => s.Negotiator).Where(s => s.Agent.UId == item.UId && (s.Status == "Снижение" || s.Status == "Удержание" || s.Status == "Рост" || s.Status == "Недозвон"));
                var agentCalls = db.sroAgents.Where(a => a.UId == item.UId).SelectMany(x => x.Sites).SelectMany(s => s.CallsTechSites).Select(c => c.Call).Distinct();
                sitesCount.Add(sroTechSitesAgent.Count());
                finishedSitesCount.Add(sroTechSitesFinishedAgent.Count());
                callsCount.Add(agentCalls.Count());
            }

            ViewBag.Agents = agents;
            ViewBag.SitesCount = sitesCount;
            ViewBag.FinishedSitesCount = finishedSitesCount;
            ViewBag.CallsCount = callsCount;

            ViewBag.AgentSitesCount = db.sroTechSites.Include(s => s.Agent).Include(s => s.Contact.Landlord).Include(s => s.Contact).Include(s => s.Negotiator).Where(s => s.Agent.UId == User.Identity.Name).Count();
            ViewBag.AgentCallsCount = db.sroAgents.Where(a => a.UId == User.Identity.Name).SelectMany(x => x.Sites).SelectMany(s => s.CallsTechSites).Select(c => c.Call).Distinct().Count();
            ViewBag.AgentTasksCount = db.sroAgents.Where(a => a.UId == User.Identity.Name).SelectMany(x => x.Sites).SelectMany(s => s.CallsTechSites).Select(c => c.Call).SelectMany(t => t.Tasks).Distinct().Count();

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}