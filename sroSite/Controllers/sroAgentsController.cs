﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using sroSite.Models;

namespace sroSite.Controllers
{
    [Authorize]
    public class sroAgentsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: sroAgents
        public async Task<ActionResult> Index()
        {
            return View(await db.sroAgents.ToListAsync());
        }

        // GET: sroAgents/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            sroAgent sroAgent = await db.sroAgents.FindAsync(id);
            if (sroAgent == null)
            {
                return HttpNotFound();
            }
            return View(sroAgent);
        }

        // GET: sroAgents/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: sroAgents/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,UId,Name,FIO,Phone")] sroAgent sroAgent)
        {
            if (ModelState.IsValid)
            {
                db.sroAgents.Add(sroAgent);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(sroAgent);
        }

        // GET: sroAgents/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            sroAgent sroAgent = await db.sroAgents.FindAsync(id);
            if (sroAgent == null)
            {
                return HttpNotFound();
            }
            return View(sroAgent);
        }

        // POST: sroAgents/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,UId,Name,FIO,Phone")] sroAgent sroAgent)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sroAgent).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(sroAgent);
        }

        // GET: sroAgents/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            sroAgent sroAgent = await db.sroAgents.FindAsync(id);
            if (sroAgent == null)
            {
                return HttpNotFound();
            }
            return View(sroAgent);
        }

        // POST: sroAgents/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            sroAgent sroAgent = await db.sroAgents.FindAsync(id);
            db.sroAgents.Remove(sroAgent);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
