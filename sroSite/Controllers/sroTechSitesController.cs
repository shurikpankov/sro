﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using sroSite.Models;

namespace sroSite.Controllers
{
    [Authorize]
    public class sroTechSitesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: sroTechSites
        public async Task<ActionResult> Index()
        {
            int page = 1;
            string search = Request["search"];
            int searchchanged = 0;

            IQueryable<sroTechSite> result = db.sroTechSites.Include(s => s.Agent).Include(s => s.Contact.Landlord).Include(s => s.Contact).Include(s => s.Negotiator);

            if (User.IsInRole("sroAgent"))
            {
                result = db.sroTechSites.Include(s => s.Agent).Include(s => s.Contact.Landlord).Include(s => s.Contact).Include(s => s.Negotiator).Where(s => s.Agent.UId == User.Identity.Name);
            }

            if (User.IsInRole("sroNegotiator"))
            {
                result = db.sroTechSites.Include(s => s.Agent).Include(s => s.Contact.Landlord).Include(s => s.Contact).Include(s => s.Negotiator).Where(s => s.Negotiator.UId == User.Identity.Name);
            }

            if (!String.IsNullOrEmpty(Request["page"]) && int.TryParse(Request["page"], out page))
            {

            }
            else
            {
                page = 1;
            }

            if (!String.IsNullOrEmpty(Request["searchchanged"]) && Request["searchchanged"] == "1")
            {
                page = 1;
            }

            if (!string.IsNullOrEmpty(search))
            {
                result = result.Where(x => x.Contact.Landlord.Name.Contains(search));
            }

            if (result != null)
            {
                ViewBag.TotalCount = result.Count();
            }
            else
            {
                ViewBag.TotalCount = 0;
            }

            result = result.OrderByDescending(x => x.Id).Skip(12 * (page - 1)).Take(12);

            var sites = result.ToList();

            List<int> dataCalls = new List<int>();
            List<int> dataCallsOpened = new List<int>();
            List<int> dataProposals = new List<int>();
            List<int> dataProposalsOpened = new List<int>();
            List<int> dataTasks = new List<int>();
            List<int> dataTasksOpened = new List<int>();
            foreach (var site in sites)
            {
                var tmpCalls = db.sroCalls.SelectMany(c => c.CallsTechSites).Where(x => x.TechSiteId == site.Id).Distinct().ToList();
                dataCalls.Add(tmpCalls != null ? tmpCalls.Count() : 0);
                var tmpOpenedCalls = tmpCalls.Where(x => x.Call.Status != "Завершен").ToList();
                dataCallsOpened.Add(tmpOpenedCalls != null ? tmpOpenedCalls.Count() : 0);
                var tmpProposals = db.sroProposals.Select(x => x.Call).SelectMany(c => c.CallsTechSites).Where(x => x.TechSiteId == site.Id).Distinct().ToList();
                dataProposals.Add(tmpProposals != null ? tmpProposals.Count() : 0);
                var tmpProposalsOpened = db.sroProposals.Where(p => p.Success == null).Select(x => x.Call).SelectMany(c => c.CallsTechSites).Where(x => x.TechSiteId == site.Id).Distinct().ToList();
                dataProposalsOpened.Add(tmpProposalsOpened != null ? tmpProposalsOpened.Count() : 0);
                var tmpTasks = db.sroTasks.Select(x => x.Call).SelectMany(c => c.CallsTechSites).Where(x => x.TechSiteId == site.Id).Distinct().ToList();
                dataTasks.Add(tmpTasks != null ? tmpTasks.Count() : 0);
                var tmpTasksOpened = db.sroTasks.Where(p => p.Success == null).Select(x => x.Call).SelectMany(c => c.CallsTechSites).Where(x => x.TechSiteId == site.Id).Distinct().ToList();
                dataTasksOpened.Add(tmpTasksOpened != null ? tmpTasksOpened.Count() : 0);
            }
            ViewBag.dataCalls = dataCalls;
            ViewBag.dataCallsOpened = dataCallsOpened;
            ViewBag.dataProposals = dataProposals;
            ViewBag.dataProposalsOpened = dataProposalsOpened;
            ViewBag.dataTasks = dataTasks;
            ViewBag.dataTasksOpened = dataTasksOpened;

            ViewBag.page = page;
            return View(await result.ToListAsync());
        }

        // GET: sroTechSites/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            sroTechSite sroTechSite = await db.sroTechSites.FindAsync(id);
            if (sroTechSite == null)
            {
                return HttpNotFound();
            }
            return View(sroTechSite);
        }

        // GET: sroTechSites/Create
        public ActionResult Create()
        {
            ViewBag.AgentId = new SelectList(db.sroAgents, "Id", "UId");
            ViewBag.ContactId = new SelectList(db.sroLandlordContacts, "Id", "Person");
            ViewBag.NegotiatorId = new SelectList(db.sroNegotiators, "Id", "UId");
            ViewBag.Landlords = new SelectList(db.sroLandlords, "Id", "Name");
            return View();
        }

        // POST: sroTechSites/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Pull,ERP,BSNum,Region,Filial,Address,Latitude,Longitude,ContractNumber,ContractDate,ContractExpire,AutoLong,AutoLongPeriod,SpecialContractTerms,PricePerMonth,PriceCompareToAvg,Tax,PaymentFrequency,Prepayment,LastChangeDate,LastChagePercent,aLowMarginSite,aDecreasedEquipment,aOftenGrowth,aLoyalClient,rImportantSite,rElectricityIsIncludedInRent,rBigAmountOfEqupment,rIncreasedAmountIfEquipment,rDiscountsOrLongPriceFreeze,rPaymentTrouble,rServiceTroubleOrOffset,Comment,Status,StatusDate,Result,SavingAmount,SavingRelative,SavingStartDate,SavingEndDate,EffectTotal,EffectYear,Effect17,Effect18,EffectRep,IsApproved,AgentId,NegotiatorId,ContactId")] sroTechSite sroTechSite)
        {
            if (ModelState.IsValid)
            {
                db.sroTechSites.Add(sroTechSite);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.AgentId = new SelectList(db.sroAgents, "Id", "UId", sroTechSite.AgentId);
            ViewBag.ContactId = new SelectList(db.sroLandlordContacts, "Id", "Person", sroTechSite.ContactId);
            ViewBag.NegotiatorId = new SelectList(db.sroNegotiators, "Id", "UId", sroTechSite.NegotiatorId);
            ViewBag.Landlords = new SelectList(db.sroLandlords, "Id", "Name");
            return View(sroTechSite);
        }

        // GET: sroTechSites/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            sroTechSite sroTechSite = await db.sroTechSites.FindAsync(id);
            if (sroTechSite == null)
            {
                return HttpNotFound();
            }
            ViewBag.AgentId = new SelectList(db.sroAgents, "Id", "FIO", sroTechSite.AgentId);
            ViewBag.NegotiatorId = new SelectList(db.sroNegotiators, "Id", "FIO", sroTechSite.NegotiatorId);
            ViewBag.LandlordId = new SelectList(db.sroLandlords, "Id", "Name", sroTechSite.Contact.LandlordId);
            ViewBag.ContactId = new SelectList(db.sroLandlordContacts, "Id", "Person", sroTechSite.ContactId);

            return View(sroTechSite);
        }

        // POST: sroTechSites/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Pull,ERP,BSNum,Region,Filial,Address,Latitude,Longitude,ContractNumber,ContractDate,ContractExpire,AutoLong,AutoLongPeriod,SpecialContractTerms,PricePerMonth,PriceCompareToAvg,Tax,PaymentFrequency,Prepayment,LastChangeDate,LastChagePercent,aLowMarginSite,aDecreasedEquipment,aOftenGrowth,aLoyalClient,rImportantSite,rElectricityIsIncludedInRent,rBigAmountOfEqupment,rIncreasedAmountIfEquipment,rDiscountsOrLongPriceFreeze,rPaymentTrouble,rServiceTroubleOrOffset,Comment,Status,StatusDate,Result,SavingAmount,SavingRelative,SavingStartDate,SavingEndDate,EffectTotal,EffectYear,Effect17,Effect18,EffectRep,IsApproved,AgentId,NegotiatorId,ContactId")] sroTechSite sroTechSite)
        {
            if (true) //!User.IsInRole("sroManager")
            {
                sroTechSite currsite = db.sroTechSites.Where(x => x.Id == sroTechSite.Id).FirstOrDefault();
                if (currsite != null)
                {
                    currsite.Pull = sroTechSite.Pull;
                    currsite.AgentId = sroTechSite.AgentId;
                    currsite.NegotiatorId = sroTechSite.NegotiatorId;
                    currsite.ContactId = sroTechSite.ContactId;
                    currsite.Comment = sroTechSite.Comment;
                    currsite.Status = sroTechSite.Status;
                    currsite.StatusDate = DateTime.Now;

                    db.Entry(currsite).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
            }

            if (ModelState.IsValid)
            {
                db.Entry(sroTechSite).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.AgentId = new SelectList(db.sroAgents, "Id", "UId", sroTechSite.AgentId);
            ViewBag.ContactId = new SelectList(db.sroLandlordContacts, "Id", "Person", sroTechSite.ContactId);
            ViewBag.NegotiatorId = new SelectList(db.sroNegotiators, "Id", "UId", sroTechSite.NegotiatorId);

            return View(sroTechSite);
        }

        // GET: sroTechSites/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            sroTechSite sroTechSite = await db.sroTechSites.FindAsync(id);
            if (sroTechSite == null)
            {
                return HttpNotFound();
            }
            return View(sroTechSite);
        }

        // POST: sroTechSites/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            sroTechSite sroTechSite = await db.sroTechSites.FindAsync(id);
            db.sroTechSites.Remove(sroTechSite);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }       
    }
}
