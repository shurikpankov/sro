﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using sroSite.Models;

namespace sroSite.Controllers
{
    [Authorize]
    public class sroNegotiatorsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: sroNegotiators
        public async Task<ActionResult> Index()
        {
            return View(await db.sroNegotiators.ToListAsync());
        }

        // GET: sroNegotiators/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            sroNegotiator sroNegotiator = await db.sroNegotiators.FindAsync(id);
            if (sroNegotiator == null)
            {
                return HttpNotFound();
            }
            return View(sroNegotiator);
        }

        // GET: sroNegotiators/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: sroNegotiators/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,UId,Name,FIO,Phone")] sroNegotiator sroNegotiator)
        {
            if (ModelState.IsValid)
            {
                db.sroNegotiators.Add(sroNegotiator);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(sroNegotiator);
        }

        // GET: sroNegotiators/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            sroNegotiator sroNegotiator = await db.sroNegotiators.FindAsync(id);
            if (sroNegotiator == null)
            {
                return HttpNotFound();
            }
            return View(sroNegotiator);
        }

        // POST: sroNegotiators/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,UId,Name,FIO,Phone")] sroNegotiator sroNegotiator)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sroNegotiator).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(sroNegotiator);
        }

        // GET: sroNegotiators/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            sroNegotiator sroNegotiator = await db.sroNegotiators.FindAsync(id);
            if (sroNegotiator == null)
            {
                return HttpNotFound();
            }
            return View(sroNegotiator);
        }

        // POST: sroNegotiators/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            sroNegotiator sroNegotiator = await db.sroNegotiators.FindAsync(id);
            db.sroNegotiators.Remove(sroNegotiator);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
