﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using sroSite.Models;

namespace sroSite.Controllers
{
    [Authorize]
    public class sroProposalsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();        

        // GET: sroProposals
        public async Task<ActionResult> Index()
        {
            int page = 1;
            string search = Request["search"];
            int searchchanged = 0;
            IQueryable<sroProposal> result = db.sroProposals.Include(s => s.Call);

            if (User.IsInRole("sroAgent"))
            {
                result = db.sroAgents.Where(a => a.UId == User.Identity.Name).SelectMany(x => x.Sites).SelectMany(s => s.CallsTechSites).Select(c => c.Call).SelectMany(t => t.Proposals).Distinct();
            }
            if (User.IsInRole("sroNegotiator"))
            {
                result = db.sroNegotiators.Where(n => n.UId == User.Identity.Name).SelectMany(x => x.Sites).SelectMany(s => s.CallsTechSites).Select(c => c.Call).SelectMany(t => t.Proposals).Distinct();                
            }

            if (!String.IsNullOrEmpty(Request["page"]) && int.TryParse(Request["page"], out page))
            {

            }
            else
            {
                page = 1;
            }

            if (!String.IsNullOrEmpty(Request["searchchanged"]) && Request["searchchanged"] == "1")
            {
                page = 1;
            }

            if (!string.IsNullOrEmpty(search))
            {
                result = result.Where(x => x.Call.CallsTechSites.FirstOrDefault().TechSite.Contact.Landlord.Name.Contains(search));
            }

            if (result != null)
            {
                ViewBag.TotalCount = result.Count();
            }
            else
            {
                ViewBag.TotalCount = 0;
            }

            result = result.OrderByDescending(x => x.Id).Skip(12 * (page - 1)).Take(12);

            ViewBag.page = page;
            return View(await result.ToListAsync());
        }

        // GET: sroProposals/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            sroProposal sroProposal = await db.sroProposals.FindAsync(id);
            if (sroProposal == null)
            {
                return HttpNotFound();
            }
            return View(sroProposal);
        }

        // GET: sroProposals/Create
        public ActionResult Create(int? callId)
        {
            //ViewBag.CallId = new SelectList(db.sroCalls, "Id", "Status");

            if (!callId.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var call = db.sroCalls.Where(x => x.Id == callId).SingleOrDefault();
            if (call == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            ViewBag.CallId = call.Id;

            return View();
        }

        // POST: sroProposals/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,DecreaseValue,DecreasePercent,DecreaseStart,DecreaseEnd,Prepayment,HasAdditionalCosts,IsLandlordProposal,Success,CallId")] sroProposal sroProposal)
        {
            if (ModelState.IsValid)
            {
                db.sroProposals.Add(sroProposal);
                await db.SaveChangesAsync();
                return RedirectToAction("Edit", "sroCalls", new { Id = sroProposal.CallId });
            }

            ViewBag.CallId = sroProposal.CallId;
            //ViewBag.CallId = new SelectList(db.sroCalls, "Id", "Id", sroProposal.CallId);
            return View(sroProposal);
        }

        // GET: sroProposals/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            sroProposal sroProposal = await db.sroProposals.FindAsync(id);
            if (sroProposal == null)
            {
                return HttpNotFound();
            }
            ViewBag.CallId = new SelectList(db.sroCalls, "Id", "Status", sroProposal.CallId);
            return View(sroProposal);
        }

        // POST: sroProposals/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,DecreaseValue,DecreasePercent,DecreaseStart,DecreaseEnd,Prepayment,HasAdditionalCosts,IsLandlordProposal,Success,CallId")] sroProposal sroProposal)
        {
            var currProposal = db.sroProposals.Where(x => x.Id == sroProposal.Id).FirstOrDefault();

            currProposal.DecreaseValue = sroProposal.DecreaseValue;
            currProposal.DecreasePercent = sroProposal.DecreasePercent;
            currProposal.DecreaseStart = sroProposal.DecreaseStart;
            currProposal.DecreaseEnd = sroProposal.DecreaseEnd;
            currProposal.Prepayment = sroProposal.Prepayment;
            currProposal.HasAdditionalCosts = sroProposal.HasAdditionalCosts;
            currProposal.IsLandlordProposal = sroProposal.IsLandlordProposal;
            currProposal.Success = sroProposal.Success;

            if (ModelState.IsValid)
            {
                db.Entry(currProposal).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.CallId = new SelectList(db.sroCalls, "Id", "Status", sroProposal.CallId);
            return View(sroProposal);
        }

        // GET: sroProposals/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            sroProposal sroProposal = await db.sroProposals.FindAsync(id);
            if (sroProposal == null)
            {
                return HttpNotFound();
            }
            return View(sroProposal);
        }

        // POST: sroProposals/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            sroProposal sroProposal = await db.sroProposals.FindAsync(id);
            db.sroProposals.Remove(sroProposal);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
