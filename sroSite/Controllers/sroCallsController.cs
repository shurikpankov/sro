﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using sroSite.Models;

namespace sroSite.Controllers
{
    [Authorize]
    public class sroCallsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: sroCalls
        public async Task<ActionResult> Index()
        {
            int page = 1;
            string search = Request["search"];
            int searchchanged = 0;
            IQueryable<sroCall> result = db.sroCalls;

            if (User.IsInRole("sroAgent"))
            {
                result = db.sroAgents.Where(a => a.UId == User.Identity.Name).SelectMany(x => x.Sites).SelectMany(s => s.CallsTechSites).Select(c => c.Call).Distinct();
            }

            if (User.IsInRole("sroNegotiator"))
            {
                result = db.sroNegotiators.Where(n => n.UId == User.Identity.Name).SelectMany(x => x.Sites).SelectMany(s => s.CallsTechSites).Select(c => c.Call).Distinct();
            }

            if (!String.IsNullOrEmpty(Request["page"]) && int.TryParse(Request["page"], out page))
            {

            }
            else
            {
                page = 1;
            }

            if (!String.IsNullOrEmpty(Request["searchchanged"]) && Request["searchchanged"] == "1")
            {
                page = 1;
            }



            if (!string.IsNullOrEmpty(search))
            {
                result = result.Where(x => x.CallsTechSites.FirstOrDefault().TechSite.Contact.Landlord.Name.Contains(search));
            }

            if (result != null)
            {
                ViewBag.TotalCount = result.Count();
            }
            else
            {
                ViewBag.TotalCount = 0;
            }

            result = result.OrderByDescending(x => x.Id).Skip(12 * (page - 1)).Take(12);

            ViewBag.page = page;

            return View(await result.ToListAsync());
        }

        public async Task<JsonResult> GetCalls(int take, int skip, string orderby)
        {
            var calls = GetData();

            var totalCount = calls.Count();

            if (!string.IsNullOrEmpty(orderby))
                calls = calls.OrderBy(x => x.EndTime);
            else
                calls = calls.OrderByDescending(x => x.Id);

            calls = calls.Skip(skip);

            if (take > 0)
                calls = calls.Take(take);

            var dtos = calls.ToList().Select(x => new
            {
                Id = x.Id,
                Status = x.Status,
                ShortDateString = x.ShortDateString,
                ShortStartTimeString = x.ShortStartTimeString,
                ShortCallLengthString = x.ShortCallLengthString,
                ImgEditTeamplate = ("<a href=!" + Url.Action("Edit", "sroCalls", new { x.Id }) + "!><img src=!/Content/images/edit.png! style=!width: 21px; height: 21px;! /></a>").Replace("!", "\"")
            });

            var result = new
            {
                items = dtos,
                totalCount = totalCount
            };

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        private IQueryable<sroCall> GetData()
        {
            if (User.IsInRole("sroManager"))
            {
                return db.sroCalls;
            }

            if (User.IsInRole("sroAgent"))
            {
                var agentCalls = db.sroAgents.Where(a => a.UId == User.Identity.Name).SelectMany(x => x.Sites).SelectMany(s => s.CallsTechSites).Select(c => c.Call).Distinct();
                return agentCalls;
            }

            if (User.IsInRole("sroNegotiator"))
            {
                var negotiatorCalls = db.sroNegotiators.Where(n => n.UId == User.Identity.Name).SelectMany(x => x.Sites).SelectMany(s => s.CallsTechSites).Select(c => c.Call).Distinct();
                return negotiatorCalls;
            }

            return db.sroCalls;
        }

        // GET: sroCalls/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            sroCall sroCall = await db.sroCalls.FindAsync(id);
            if (sroCall == null)
            {
                return HttpNotFound();
            }
            return View(sroCall);
        }

        // GET: sroCalls/Create
        public ActionResult Create(int? siteId)
        {
            if (!siteId.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var site = db.sroTechSites.Where(x => x.Id == siteId).SingleOrDefault();
            if (site == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            //var sites = db.sroTechSites.Where(x => x.ContactId == site.ContactId).ToList();
            //ViewBag.SameSites = new SelectList(sites, "Id", "ERP", site.Id);

            var sites = db.sroTechSites.Where(x => x.AgentId == site.AgentId).Where(x => x.ContactId == site.ContactId).ToList();
            var otherContactSites = db.sroTechSites.Where(x => x.AgentId == site.AgentId).Where(x => x.Contact.LandlordId == site.Contact.LandlordId).Where(x => x.ContactId != site.ContactId).ToList();
            var otherAgentSites = db.sroTechSites.Where(x => x.AgentId != site.AgentId).Where(x => x.Contact.LandlordId == site.Contact.LandlordId).Where(x => x.ContactId == site.ContactId).ToList();
            var otherSites = db.sroTechSites.Where(x => x.AgentId != site.AgentId).Where(x => x.Contact.LandlordId == site.Contact.LandlordId).Where(x => x.ContactId != site.ContactId).ToList();

            ViewBag.SameSites = new SelectList(sites, "Id", "ERP", site.Id);
            ViewBag.OtherContactSites = new SelectList(otherContactSites, "Id", "ERP", site.Id);
            ViewBag.OtherAgentSites = new SelectList(otherAgentSites, "Id", "ERP", site.Id);
            ViewBag.OtherSites = new SelectList(otherSites, "Id", "ERP", site.Id);

            return View();
        }

        // POST: sroCalls/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(sroCall sroCall)
        {
            if (ModelState.IsValid)
            {
                sroCall.Status = "Подготовка";

                db.sroCalls.Add(sroCall);

                var siteIds = Request["sites"].Split(',').Select(x => Convert.ToInt32(x)).ToList();

                for (int i = 0; i < siteIds.Count; i++)
                {
                    sroCallsTechSites SiteForCall = new sroCallsTechSites();
                    SiteForCall.CallId = sroCall.Id;
                    SiteForCall.TechSiteId = siteIds[i];
                    db.sroCallsTechSites.Add(SiteForCall);
                }

                await db.SaveChangesAsync();
                return RedirectToAction("Edit/" + sroCall.Id);
                //return RedirectToAction("Index");
            }
            
            return new HttpStatusCodeResult(HttpStatusCode.MethodNotAllowed);
        }

        // GET: sroCalls/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            sroCall sroCall = await db.sroCalls.FindAsync(id);
            if (sroCall == null)
            {
                return HttpNotFound();
            }

            //var agentCalls = db.sroAgents.Where(a => a.UId == User.Identity.Name).SelectMany(x => x.Sites).SelectMany(s => s.CallsTechSites).Select(c => c.Call).Distinct();

            var sites = db.sroTechSites.SelectMany(x => x.CallsTechSites).Where(c => c.CallId == sroCall.Id).ToList();

            //var sites = db.sroTechSites.Where(x => x.AgentId == site.AgentId).Where(x => x.ContactId == site.ContactId).ToList();
            ViewBag.SameSites = new SelectList(sites, "Id", "ERP");
            ViewBag.Landlord = sites.FirstOrDefault().TechSite.Contact.Landlord;
            ViewBag.Contact = sites.FirstOrDefault().TechSite.Contact;

            return View(sroCall);
        }

        // POST: sroCalls/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Status,StatTime,EndTime,LandlordReached,ContactId,argGeneral,argNationwide,argMarket,argLowMargin,argNetwork,argPriceDynamic,Comment")] sroCall sroCall)
        {
            if (ModelState.IsValid)
            {
                string comment = sroCall.Comment;
                //bool? llreached = sroCall.LandlordReached;
                bool argGeneral = sroCall.argGeneral;
                bool argLowMargin = sroCall.argLowMargin;
                bool argMarket = sroCall.argMarket;
                bool argNationwide = sroCall.argNationwide;
                bool argNetwork = sroCall.argNetwork;
                bool argPriceDynamic = sroCall.argPriceDynamic;

                sroCall = db.sroCalls.Where(x => x.Id == sroCall.Id).FirstOrDefault();

                if (sroCall.Status == "Подготовка")
                {
                    sroCall.Status = "Переговоры";
                    sroCall.StatTime = DateTime.Now;
                }
                else
                {
                    if (sroCall.Status == "Переговоры")
                    {
                        bool reached = false;

                        if (!string.IsNullOrEmpty(Request["Complete"]) && Request["Complete"] == "success")
                        {
                            reached = true;
                        }

                        
                        sroCall.EndTime = DateTime.Now;
                        sroCall.Status = "Отчет";

                        if (reached == true)
                        {
                            sroCall.LandlordReached = true;
                        }
                        else
                        {
                            sroCall.LandlordReached = false;
                        }
                    }
                    else
                    {
                        if (sroCall.Status == "Отчет")
                        {
                            sroCall.Status = "Завершен";
                            sroCall.Comment = comment;
                            //sroCall.LandlordReached = llreached;
                            sroCall.argGeneral = argGeneral;
                            sroCall.argLowMargin = argLowMargin;
                            sroCall.argMarket = argMarket;
                            sroCall.argNationwide = argNationwide;
                            sroCall.argNetwork = argNetwork;
                            sroCall.argPriceDynamic = argPriceDynamic;
                        }
                    }
                }
                db.Entry(sroCall).State = EntityState.Modified;

                var contact = db.sroTechSites.SelectMany(x => x.CallsTechSites).Where(c => c.CallId == sroCall.Id).FirstOrDefault().TechSite.Contact;

                if (sroCall.LandlordReached == true)
                {
                    contact.IsActual = true;
                    contact.ActualizationDate = DateTime.Now;
                    db.Entry(contact).State = EntityState.Modified;
                }
                await db.SaveChangesAsync();
                return RedirectToAction("Edit/" + sroCall.Id);
            }
            return View(sroCall);
        }

        /*
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> StartCall([Bind(Include = "Id,Status,StatTime,EndTime,LandlordReached,Comment")] sroCall sroCall)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sroCall).State = EntityState.Modified;
                sroCall.Status = "Переговоры";
                sroCall.StatTime = DateTime.Now;
                await db.SaveChangesAsync();
                return RedirectToAction("Edit/"+sroCall.Id);
            }
            return View(sroCall);
        }*/

        // GET: sroCalls/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            sroCall sroCall = await db.sroCalls.FindAsync(id);
            if (sroCall == null)
            {
                return HttpNotFound();
            }
            return View(sroCall);
        }

        // POST: sroCalls/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            sroCall sroCall = await db.sroCalls.FindAsync(id);
            db.sroCalls.Remove(sroCall);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
