﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using sroSite.Models;

namespace sroSite.Controllers
{
    [Authorize]
    public class sroLandlordContactsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: sroLandlordContacts
        public async Task<ActionResult> Index()
        {
            int landlordId;
            int page = 1;
            string search = Request["search"];
            int searchchanged = 0;
            IQueryable<sroLandlordContact> result;

            if (!String.IsNullOrEmpty(Request["page"]) && int.TryParse(Request["page"], out page))
            {

            }
            else
            {
                page = 1;
            }

            if (!String.IsNullOrEmpty(Request["searchchanged"]) && Request["searchchanged"] == "1")
            {
                page = 1;
            }

            if (int.TryParse(Request["landlordId"], out landlordId))
            {
                var sroContactsOfLandlord = db.sroLandlordContacts.Include(s => s.Landlord).Where(x => x.LandlordId == landlordId);
                return View(await sroContactsOfLandlord.ToListAsync());
            }
            else
            {
                if (!string.IsNullOrEmpty(search))
                {
                    result = db.sroLandlordContacts.Include(s => s.Landlord).Where(x => x.Landlord.Name.Contains(search));
                }
                else
                {
                    result = db.sroLandlordContacts.Include(s => s.Landlord);
                }
            }

            if (result != null)
            {
                ViewBag.TotalCount = result.Count();
            }
            else
            {
                ViewBag.TotalCount = 0;
            }
            result = result.OrderByDescending(x => x.Id).Skip(12 * (page - 1)).Take(12);
            //var sroLandlordContacts = db.sroLandlordContacts.Include(s => s.Landlord);
            ViewBag.page = page;
            return View(await result.ToListAsync());
        }

        public async Task<JsonResult> GetByLandlordId(int? llId)
        {
            var sroLandlordContacts = db.sroLandlordContacts.Include(s => s.Landlord).Where(s => s.LandlordId == llId);
            return Json(new SelectList(await sroLandlordContacts.ToArrayAsync(), "ID", "Person"), JsonRequestBehavior.AllowGet);
            //return View(await sroLandlordContacts.ToListAsync());
        }

        // GET: sroLandlordContacts/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            sroLandlordContact sroLandlordContact = await db.sroLandlordContacts.FindAsync(id);
            if (sroLandlordContact == null)
            {
                return HttpNotFound();
            }
            return View(sroLandlordContact);
        }

        // GET: sroLandlordContacts/Create
        public ActionResult Create()
        {
            ViewBag.LandlordId = new SelectList(db.sroLandlords, "Id", "Name");
            return View();
        }

        // POST: sroLandlordContacts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Person,Position,PhoneCell,PhoneCity,Email,Address,PostAddress,IsActual,ActualizationDate,LandlordId,Comment")] sroLandlordContact sroLandlordContact)
        {
            if (ModelState.IsValid)
            {
                db.sroLandlordContacts.Add(sroLandlordContact);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.LandlordId = new SelectList(db.sroLandlords, "Id", "Name", sroLandlordContact.LandlordId);
            return View(sroLandlordContact);
        }

        // GET: sroLandlordContacts/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            sroLandlordContact sroLandlordContact = await db.sroLandlordContacts.FindAsync(id);
            if (sroLandlordContact == null)
            {
                return HttpNotFound();
            }
            ViewBag.LandlordId = new SelectList(db.sroLandlords, "Id", "Name", sroLandlordContact.LandlordId);
            return View(sroLandlordContact);
        }

        // POST: sroLandlordContacts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Person,Position,PhoneCell,PhoneCity,Email,Address,PostAddress,IsActual,ActualizationDate,LandlordId,Comment")] sroLandlordContact sroLandlordContact)
        {
            if (ModelState.IsValid)
            {
                sroLandlordContact.ActualizationDate = DateTime.Now;
                db.Entry(sroLandlordContact).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.LandlordId = new SelectList(db.sroLandlords, "Id", "Name", sroLandlordContact.LandlordId);
            return View(sroLandlordContact);
        }

        // GET: sroLandlordContacts/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            sroLandlordContact sroLandlordContact = await db.sroLandlordContacts.FindAsync(id);
            if (sroLandlordContact == null)
            {
                return HttpNotFound();
            }
            return View(sroLandlordContact);
        }

        // POST: sroLandlordContacts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            sroLandlordContact sroLandlordContact = await db.sroLandlordContacts.FindAsync(id);
            db.sroLandlordContacts.Remove(sroLandlordContact);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
